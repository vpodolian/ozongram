export default class PostCreator {

    createPost(options, postCreatedCallback) {
        if (!options) {
            console.error("options aren't specified!");
            return;
        }

        if (!options.container) {
            console.error("container isn't specified!");
            return;
        }

        if (!options.data) {
            console.error("data isn't specified!");
        }

        const post = document.createElement('div');
        post.className = 'post';

        post.appendChild(this.createPostHeader(options.data));
        post.appendChild(this.createPostImage(options.data.images.low_resolution.url));
        post.appendChild(this.createPostDescription(options.data));

        const li = document.createElement('li');
        li.appendChild(post);

        options.container.appendChild(li);

        if (typeof postCreatedCallback === 'function')
            postCreatedCallback();
    }

    createPostHeader(postData) {
        const userInfoDiv = this.createUserInfo(postData.user);
        if (postData.location)
            userInfoDiv.appendChild(this.createLocationLabel(postData.location));

        const div = document.createElement('div');
        div.className = 'post-header';
        div.appendChild(this.createUserAvatar(postData.user.profile_picture));
        div.appendChild(userInfoDiv);

        return div;
    }

    createUserAvatar(avatarUrl) {
        const img = document.createElement('img');
        img.src = avatarUrl;

        const a = document.createElement('a');
        a.className = 'user-avatar';
        a.appendChild(img);

        return a;
    }

    createUserInfo(user) {
        const userInfoDiv = document.createElement('div');
        userInfoDiv.className = 'user-info';

        const h2 = document.createElement('h2');
        h2.innerHTML = user.username;
        userInfoDiv.appendChild(h2);

        return userInfoDiv;
    }

    createLocationLabel(location) {
        if (!location) return;

        const div = document.createElement('div');
        div.innerHTML = location.name;
        return div;
    }

    createPostImage(imgUrl) {
        const img = document.createElement('img');
        img.src = imgUrl;

        const a = document.createElement('a');
        a.className = 'post-image';
        a.appendChild(img);

        return a;
    }

    createLikesBox(likesCount, onHeartClick) {
        const div = document.createElement('div');
        div.className = 'likes-box';

        const heartIcon = document.createElement('i');
        heartIcon.className = 'fa fa-heart-o like-heart';
        heartIcon.addEventListener('click', onHeartClick);
        div.appendChild(heartIcon);

        const span = document.createElement('span');
        span.className = 'like-count'
        span.innerHTML = likesCount;
        div.appendChild(span);

        return div;
    }

    createPostDescription(postData) {
        const div = document.createElement('div');
        div.className = 'post-description';

        div.appendChild(
            this.createLikesBox(postData.likes.count,
            () => alert(postData.id)));

        const p = document.createElement('p');
        p.innerHTML = postData.caption.text;
        div.appendChild(p);

        return div;
    }

}
