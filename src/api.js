import jsonp from 'jsonp';

export default class API {
    constructor(apiKey) {
        this.key = apiKey;
    }

    getRecentImages(userId, count, fnCallback) {
        const url = `https://api.instagram.com/v1/users/${userId}/media/recent?access_token=${this.key}&count=${count}`;
        jsonp(url,undefined, (err, data) => {
            if (err) console.error(err);
            fnCallback(data);
        });
    }
}
