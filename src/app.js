import API from './api.js';
import PostCreator from './postCreator.js';

import './css/style.css';
import './css/style-mobile.css';
import './css/style-tablet.css';

const apiKey = '691623.1419b97.479e4603aff24de596b1bf18891729f3';
const api = new API(apiKey);

const userId = 691623;
const imgCount = 20;

api.getRecentImages(userId, imgCount, respData => {
    console.log(respData);
    if (respData.length == 0) return;

    if (respData.meta && respData.meta.code != 200) {
        console.error(`Post fetching error. Code: ${respData.meta.code}. Message: ${respData.meta.error_message}`);
    }

    const list = document.querySelector('.posts-list');
    const creator = new PostCreator();

    respData.data.forEach(item => creator.createPost({ container: list, data: item }));
});
