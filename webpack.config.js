var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    './src/app'
  ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'dist/bundle.js',
    publicPath: ''
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: "./index.html"
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel' ],
        exclude: /node_modules/,
        include: __dirname
      },
      {
         test: /\.css$/,
         loader: ('style!css')
      }
    ]
 },
 devServer: {
     contentBase: './'
 }
}
